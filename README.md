# Workshop front-end Via Varejo

Logs com assuntos discutidos sobre desenvolvimento front-end e boas práticas.

---

## #05.03.18

[Layout](https://codepen.io/wilkerviana/pen/OvMgqo) básico HTML como referência para desenvolvimento abordando os seguintes tópicos:

1. Semântica das tags
2. Acessibilidade
3. SEO
4. Legibilidade
5. Performance

HTML5 - Evita retrabalho de código e faz com que a Web retome o rumo da semântica e código enxuto.

***Header*** - Representa o conteúdo introdutório. Pode conter elementos de título, logotipo, formulário de pesquisa e etc...

***Aside*** - Representa uma parte do documento em que seu conteúdo está indiretamente relacionado ao conteúdo principal.

***Section*** - Representa uma seção autonôma. Exibição de elementos não específicos podem ser colocados dentro de uma section.

***Article*** - Representa uma composição autonôma do documento, página, aplicativo ou site que se destina a ser independente ou reutilizável.

***Footer*** - Representa um rodapé para o conteúdo de seção mais próximo ou elemento raiz da sessão.

***Nav*** - Representa uma seção de uma página cujo objetivo é fornecer links de navegação.

***Width*** - o atributo width foi descontinuado porque modifica a formatação do elemento e suas funções são melhores controladas pelo CSS.

Links de referência: **[devdocs.io](http://devdocs.io/)**

*Pesquisa referente a tags: Header / Aside / Section / Article / Footer / Nav*

*Pesquisa referente a descontinuidade do atributo Width*

---

## #07.03.18

Discussão sobre tipos de documento HTML:

1. Doctype e xhtml
2. Padronização HTML&CSS (W3C)
3. Tag MAIN
4. Camadas do front-end

***Doctype*** - mostra para o browser que tipo de documento está lendo, se é XHTML, HTML4, HTML5 e etc...

***Main*** - é nela que se encontra o conteúdo principal. Só pode ter apenas um main na página. Baixa compatibilidade, pois engines mais antigas não reconhecem esta tag.

***Camadas do Front-end***

Conteúdo      -> HTML = usuário vê

Layout        -> CSS  = parte estética

Comportamento -> JS = Interação com usuário

Links de referência: **[HTML5Doctor](http://html5doctor.com/)** e **[CanIUse](https://caniuse.com/)**

*Pesquisa referente a tags: Footer / Nav / Address*

---

## #12.03.18

Discussão sobre as tags anteriores:

1. Microformats
2. Tag Address
3. Tag Footer
4. Tag Nav
5. **[GetNinjas](https://www.getninjas.com.br/)** como referência de uso da tag Address
6. Sintaxe básica do CSS para o layout proposto no primeiro encontro

***Microformats*** - padronização através de classes para que buscadores tenham facilidade para leitura do conteúdo.

***Address*** - nesta tag pode conter qualquer tipo de informação de contato como endereço, url, e-mail, telefone, mídia social, localização e etc...

*Sintaxe básica do CSS*

```
selector{
  property: value
}
```

*Pesquisa referente a: BoxModel / Seletores CSS*

---

## #14.03.18

Discussão sobre box model e resets css:

1. Como funciona o box-model e suas propriedades.
2. CSS como funciona?!
3. Resets e Normalize: O que é e como funciona?!
4. Seletores CSS

***Box-Model*** - trata-se de como as 4 propriedades (conteúdo, espaçamento, bordas e margins) se relacionam para compor a dimensão de um elemento.

Utilizamos a propriedade *box-sizing* para alterar a forma como o browser interpreta o box-model.

> O browser por padrão utiliza o valor content-box

Para ajudar no entendimento, vale a pena o material da **[MDN](https://developer.mozilla.org/pt-BR/docs/Web/CSS/box_model)**

***Resets*** - Não é aconselhável resetar o estilo utilizando o *, pois propriedades escolhidas serão definidas em todos elementos e isso irá afetar prejudicialmente a performance da aplicação. É aconselhável aplicar o reset diretamente na tag específica que deseja obter o estilo.
O browser já tem um estilo padrão para os elementos do html,e é por isso que utilizamos técnicas de reset.

***Seletores CSS***

Alguns seletores CSS:
```
p {
  *seletor de tag*
}
#paragraph {
  *seletor de id*
}
.paragraph {
  *seletor de classe*
}
p:first-child {
  *seletor de pseudo-classe*
}
p:before , p:after {
  *seletor de pseudo-elemento*
}
p[attr="attr"] {
  *seletor de atributo*
}
```

Links de referência: **[ResetCSS por Eric Meyer](https://meyerweb.com/eric/tools/css/reset/)** e **[Normalize CSS](http://nicolasgallagher.com/about-normalize-css/)**

---

## #19.03.18

Discussão sobre fontes, proporção e unidades de medida:

1. Qual a melhor maneira de definir tamanho de fonte?
2. O que são pixels?
3. Unidades de medida CSS - absolutas x relativas

***Definindo tamanho de fontes*** - Devemos utilizar sempre unidades relativas para tamanho de fonte e basear nosso HTML com propriedades que trazem o valor que o usuário implementa no browser.
Aprendemos que utilizar *px* como medida em tamanho de fonte, atrapalha o processo de Web Responsiva. Exemplo:
***16px*** serão ***16px*** em qualquer dispositivo. Ocupará o mesmo espaço tanto em um celular quanto em uma TV 4K.

***Unidades de medidas CSS*** - Aprendemos que temos muitas unidades de medidas e que a utilização de cada uma depende do seu projeto. Por isso sempre devemos analisar o projeto e o contexto como um todo e tomar decisões que fazem sentido baseados na realidade em que não sabemos em qual dispositivo o nosso usuário fará uso do nosso site/aplicação.

* **em**: Unidade relativa ao elemento pai
* **rem**: Unidade relativa ao root do elemento
* **vw**: Unidade relativa a largura da viewport
* **vh**: Unidade relativa a altura da viewport
* **%**: Unidade relativa ao elemento pai

Links de referência: **[Unidades de medida CSS](http://www.maujor.com/tutorial/unidades-de-medidas-css.php)** e **[O que são pixels?](https://mundoestranho.abril.com.br/tecnologia/o-que-e-um-pixel/)**